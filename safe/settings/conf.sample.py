"""
Configuration file for settings
"""

SECRET_KEY = '<SECRET KEY>'

ALLOWED_HOSTS = ['*']

DB_NAME = 'safe'

DB_USERNAME = 'safe'

DB_PASSWORD = 'safe'

DB_HOST_NAME = 'localhost'

DB_PORT = '5432'

ADMINS_EMAIL_LIST = [
    # ('Name', 'email@example.com'),
]
